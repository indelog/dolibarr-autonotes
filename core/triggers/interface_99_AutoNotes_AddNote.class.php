<?php
/* Copyright (C) Maxime DEMAREST <maxime@indelog.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    core/triggers/interface_99_AutoNotes_AddNote.class.php
 * \ingroup AutoNotes
 * \brief   Trigger for auto add note.
 *
 * Put detailed description here.
 *
 */

require_once DOL_DOCUMENT_ROOT.'/core/triggers/dolibarrtriggers.class.php';


/**
 *  Class of triggers for MyModule module
 */
class InterfaceAddNote extends DolibarrTriggers
{
    /**
     * @var DoliDB Database handler
     */
    protected $db;

    /**
     * Constructor
     *
     * @param DoliDB $db Database handler
     */
    public function __construct($db)
    {
        $this->db = $db;

        $this->name = preg_replace('/^Interface/i', '', get_class($this));
        $this->family = "autonotes";
        $this->description = "Auto add note for some element create/update triggers.";
        // 'development', 'experimental', 'dolibarr' or version
        $this->version = 'dolibarr';
        $this->picto = 'autonotes@autonotes';
    }

    /**
     * Trigger name
     *
     * @return string Name of trigger file
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Trigger description
     *
     * @return string Description of trigger file
     */
    public function getDesc()
    {
        return $this->description;
    }


    /**
     * Function called when a Dolibarrr business event is done.
     * All functions "runTrigger" are triggered if file
     * is inside directory core/triggers
     *
     * @param string 		$action 	Event action code
     * @param CommonObject 	$object 	Object
     * @param User 			$user 		Object user
     * @param Translate 	$langs 		Object langs
     * @param Conf 			$conf 		Object conf
     * @return int              		<0 if KO, 0 if no triggered ran, >0 if OK
     */
    public function runTrigger($action, $object, User $user, Translate $langs, Conf $conf)
    {
        if (empty($conf->autonotes->enabled)) return 0;     // If module is not enabled, we do nothing

        require_once __DIR__.'/../../lib/autonotes.lib.php';
        require_once __DIR__.'/../../class/note.class.php';

        switch ($action) {
            // setup some vars depending of trigger caller
            case 'LINEORDER_INSERT':
            case 'LINEORDER_DELETE':
                $context = 'commande';
                require_once DOL_DOCUMENT_ROOT.'/commande/class/commande.class.php';
                $lineowner = new Commande($this->db);
                break;
            case 'LINEORDER_SUPPLIER_CREATE':
            case 'LINEORDER_SUPPLIER_DELETE':
                $context  = 'commande_fournisseur';
                require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.commande.class.php';
                $lineowner = new CommandeFournisseur($this->db);
                break;
            case 'LINEBILL_INSERT':
            case 'LINEBILL_DELETE':
                $context = 'facture';
                require_once DOL_DOCUMENT_ROOT.'/compta/facture/class/facture.class.php';
                $lineowner = new Facture($this->db);
                break;
            case 'LINEBILL_SUPPLIER_CREATE':
            case 'LINEBILL_SUPPLIER_DELETE':
                $context = 'facture_fourn';
                require_once DOL_DOCUMENT_ROOT.'/fourn/class/fournisseur.facture.class.php';
                $lineowner = new FactureFournisseur($this->db);
                break;
            case 'LINEPROPAL_INSERT':
            case 'LINEPROPAL_DELETE':
                $context = 'propal';
                require_once DOL_DOCUMENT_ROOT.'/comm/propal/class/propal.class.php';
                $lineowner = new Propal($this->db);
                break;
        }

        switch ($action) {
            /*
             * Line insert case
             * Autonote insered is dependent of the product categorie
             */
            case 'LINEORDER_INSERT':
            case 'LINEORDER_SUPPLIER_CREATE':
            case 'LINEBILL_INSERT':
            case 'LINEBILL_SUPPLIER_CREATE':
            case 'LINEPROPAL_INSERT';
                dol_syslog("Trigger '".$this->name."' for action '$action' launched by ".__FILE__.". id=".$object->id);

                // If the module categories not enable, do not autoadd autonotes base on thirdpary categories
                if (!$conf->categorie->enabled)
                {
                    if (empty($conf->global->AUTONOTE_REMOVE_CATEG_MOD_NOT_ENBALED_WARN))
                        dol_syslog(__METHOD__.' autonotes was not automatically added because the module Categorie was not enabled.', LOG_WARNING);
                    return 0;
                }

                // Product with id less of 1 is free line and has no categories
                if ($object->fk_product > 0)
                {
                    // Retrive all the categories which the product is member
                    require_once DOL_DOCUMENT_ROOT.'/categories/class/categorie.class.php';
                    $categ_static = new Categorie($this->db);
                    $prodcategs = $categ_static->containing($object->fk_product, Categorie::TYPE_PRODUCT, 'id');
                    // iterate on each categs of the product
                    if (empty($prodcategs))
                    {
                        // We no need to contine if the product has no categories
                        dol_syslog(__METHOD__.' Product with id='.$object->fk_product.' has  no categories', LOG_DEBUG);
                        return 0;
                    }

                    $note_static = new Note($this->db);
                    $notecategs_field = 'auto_categs_product_'.$context;

                    // check if the categs fields are managed by autonotes
                    if (!in_array($notecategs_field, $note_static::CATEGS_FIELDS))
                    {
                        dol_syslog(__METHOD__.' typenote \''.$notecategs_field.'\' not in Note::CATEGS_FIELDS.', LOG_ERR);
                        return -4;
                    }

                    // We must fetch the object which own the line (not directly accesible in the trigger)
                    if ($lineowner->fetch($object->{$lineowner->fk_element}) < 1)
                    {
                        dol_syslog(__METHOD__.'Can\'t fetch object that own this line.', LOG_ERR);
                        return -5;
                    }

                    // Get the list of all autonotes associated with this note categorie field
                    $sql = 'SELECT rowid AS id, '.$notecategs_field.' AS categs, type FROM '.MAIN_DB_PREFIX.$note_static->table_element.' WHERE status = 1';
                    $resql = $this->db->query($sql);
                    if (!$resql)
                    {
                        dol_syslog(__METHOD__.' faild to execute query to fetch autonotes.', LOG_ERR);
                        return -6;
                    }

                    $arr_autonotes = array();
                    while ($res = $this->db->fetch_object($resql)) {
                        $arr_autonotes[$res->id] = array();
                        $arr_autonotes[$res->id]['categs'] = explode(',', $res->categs);
                        $arr_autonotes[$res->id]['type'] = Note::MAP_TYPE_NOTE[$res->type];
                    }

                    // Split into suitable array the autonotes ids which the line owner already have
                    $lineowner_autonotes = array(
                        'public' => empty($lineowner->array_options['options_autonotes_public']) ? array() : explode(',', $lineowner->array_options['options_autonotes_public']),
                    );

                    // List each ids of added autonotes (used to list automatically added autonotes)
                    $autonotes_ids_added = array(
                        'public' => array(),
                    );

                    // Iterate on each categorie of product to find if we must join an autonote to the line onwer
                    foreach ($prodcategs as $pcateg)
                    {
                        // iterate on each fetched autonotes
                        foreach ($arr_autonotes as $an_id => $an_val)
                        {
                            // Is the product categorie present in the autonotes categories ?
                            if (in_array($pcateg, $an_val['categs']))
                            {
                                // For now ony public type is keeped, other type will be added leather
                                foreach (Note::MAP_TYPE_NOTE as $note_type)
                                {
                                    // Check if the line owner already has the autonote and avoid to adding it in duplicate
                                    if (!empty($lineowner_autonotes[$note_type]) && in_array($an_id, $lineowner_autonotes[$note_type]))
                                    {
                                        dol_syslog(__METHOD__.' the categories for product with id='.$object->fk_product.' match with autonotes id='.$an_id.' and type='.$an_val['type'].' for categs field'.$notecategs_field.': the object already had this autonote, do not re-add id.', LOG_DEBUG);
                                        continue;
                                    }
                                    else
                                    {
                                        dol_syslog(__METHOD__.' the categories for product with id='.$object->fk_product.' match with autonotes id='.$an_id.' and type='.$an_val['type'].' for categs field'.$notecategs_field.' : add the autonote to this object.');
                                        // This is used to update the extrafields
                                        $lineowner_autonotes[$note_type][] = $an_id;
                                        // Explicitly register the ids of added autonote
                                        $autonotes_ids_added[$note_type][] = $an_id;
                                    }
                                }
                            }
                        }
                    }

                    // If an new autonote is added update the related extrafields and notes
                    if (!empty($autonotes_ids_added['public']))
                    {
                        $lineowner->array_options['options_autonotes_public'] = implode(',', $lineowner_autonotes['public']);
                        $res = $lineowner->updateExtraField('autonotes_public');
                        if ($res < 1) dol_syslog(__METHOD__.' update extrafield '.'autonotes_public failed', LOG_ERR);
                        else autoNoteUpdateNoteField($lineowner, $autonotes_ids_added);
                    }
                }
                else
                {
                    dol_syslog(__METHOD__.' This is a free line with no product id.', LOG_DEBUG);
                }
                return 1;
                break;

            /*
             * Object create or modify case
             * Modify the note field function the manually added autonotes
             */
            case 'ORDER_CREATE':
            case 'ORDER_MODIFY':
            case 'ORDER_SUPPLIER_CREATE':
            case 'ORDER_SUPPLIER_MODIFY':
            case 'PROPAL_CREATE':
            case 'PROPAL_MODIFY':
            case 'BILL_CREATE':
            case 'BILL_MODIFY':
            case 'BILL_SUPPLIER_CREATE':
            case 'BILL_SUPPLIER_UPDATE':
            case 'SHIPPING_CREATE':
            case 'SHIPPING_MODIFY':
                dol_syslog("Trigger '".$this->name."' for action '$action' launched by ".__FILE__.". id=".$object->id);
                autoNoteUpdateNoteField($object);
                /*
                 * extra field update do'nt regenerate the documnent
                 * we need to do this for the new autonote apperars
                 */
                // Get the list of extrafield attrubute used by autonotes and only do the gen doc if it's one of those
                $extras_attribute = array_map(function($val) {return 'autonotes_'.$val;}, Note::MAP_TYPE_NOTE);
                if (($object->context['extrafieldupdate'] || $object->context['extrafieldaddupdate']) && in_array(GETPOST('attribute', 'aZ09'), $extras_attribute) && empty($conf->global->MAIN_DISABLE_PDF_AUTOUPDATE) )
                {
                    // Normally this is set on the card but it not forwarded in the trigger
                    $hidedetails = (GETPOST('hidedetails', 'int') ? GETPOST('hidedetails', 'int') : (!empty($conf->global->MAIN_GENERATE_DOCUMENTS_HIDE_DETAILS) ? 1 : 0));
                    $hidedesc = (GETPOST('hidedesc', 'int') ? GETPOST('hidedesc', 'int') : (!empty($conf->global->MAIN_GENERATE_DOCUMENTS_HIDE_DESC) ? 1 : 0));
                    $hideref = (GETPOST('hideref', 'int') ? GETPOST('hideref', 'int') : (!empty($conf->global->MAIN_GENERATE_DOCUMENTS_HIDE_REF) ? 1 : 0));
                    // Define output language
                    $outputlangs = $langs;
                    $newlang = '';
                    if ($conf->global->MAIN_MULTILANGS && empty($newlang) && GETPOST('lang_id', 'aZ09')) $newlang = GETPOST('lang_id', 'aZ09');
                    if ($conf->global->MAIN_MULTILANGS && empty($newlang)) $newlang = $object->thirdparty->default_lang;
                    if (!empty($newlang))
                    {
                        $outputlangs = new Translate("", $conf);
                        $outputlangs->setDefaultLang($newlang);
                    }
                    $object->generateDocument($object->model_pdf, $outputlangs, $hidedetails, $hidedesc, $hideref);
                }
                return 1;
                break;

            /*
             * On line deletion
             * Remove a autonote if it autoadded and no other product requier it
             */
            case 'LINEORDER_DELETE':
            case 'LINEORDER_SUPPLIER_DELETE':
            case 'LINEBILL_DELETE':
            case 'LINEBILL_SUPPLIER_DELETE':
            case 'LINEPROPAL_DELETE':
                dol_syslog("Trigger '".$this->name."' for action '$action' launched by ".__FILE__.". id=".$object->id);

                // If the module categories not enable, do not autoremove autonote based on product categories
                if (!$conf->categorie->enabled)
                {
                    if (empty($conf->global->AUTONOTE_REMOVE_CATEG_MOD_NOT_ENBALED_WARN))
                        dol_syslog(__METHOD__.' autonotes was not automatically added because the module Categorie was not enabled.', LOG_WARNING);
                    return 0;
                }

                // Retrive the line owner
                if ($lineowner->fetch($object->{$lineowner->fk_element}) < 1)
                {
                    dol_syslog(__METHOD__.'Can\'t fetch object that own this line.', LOG_ERR);
                    return -5;
                }

                $used_autonotes_ids = array();

                // do nothing if object has no autonotes
                $has_autnote = false;
                // TODO replace this array whith
                foreach (Note::MAP_TYPE_NOTE as $note_type)
                {
                    if (!empty($lineowner->array_options['options_autonotes_'.$note_type.'_html']))
                    {
                        $has_autonote = true;
                        $used_autonotes_ids[$note_type] = array();
                    }
                }
                if (!$has_autonote)
                {
                    dol_syslog(__METHOD__.' this object does\'nt have autonotes');
                    return 0;
                }

                // Search on each product line the active attached autonotes
                foreach ($lineowner->lines as $line)
                {
                    $autonotes_for_obj = autoNoteGetForObj($line->fk_product, $this->db, 'product', $context, false);
                    foreach (Note::MAP_TYPE_NOTE as $note_type)
                    {
                        $used_autonotes_ids[$note_type] = array_unique(array_merge($used_autonotes_ids[$note_type], $autonotes_for_obj['options_autonotes_'.$note_type]));
                    }
                }

                $update_autonote = false;

                // Get the list of autonotes automatically added on this object
                $autoadded_autonotes_ids = array();
                $manuadded_autonotes_ids = array();
                foreach (Note::MAP_TYPE_NOTE as $note_type)
                {
                    $autoadded_autonotes_ids[$note_type] = array();
                    $manuadded_autonotes_ids[$note_type] = array();
                    $note_elements = preg_split('/<!--AUTONOTES_BEGIN-->|<!--AUTONOTES_END-->/', html_entity_decode($lineowner->array_options['options_autonotes_'.$note_type.'_html']));
                    foreach($note_elements as $elem)
                    {
                        if (preg_match('/^<!--AUTONOTES_NOTEID_([0-9]+)_([A-Z]*)-->/', html_entity_decode($elem), $n_params))
                        {
                            $n_id = $n_params[1];
                            $n_added = $n_params[2];
                            if ($n_added === 'AUTO') $autoadded_autonotes_ids[$note_type][] = (string) $n_id;
                            else if ($n_added === 'MANU') $manuadded_autonotes_ids[$note_type][] = (string) $n_id;
                        }
                    }
                    $new_autonotes_field = array_unique(array_merge($manuadded_autonotes_ids[$note_type], $used_autonotes_ids[$note_type]));
                    $actual_autonotes_field = explode(',', $lineowner->array_options['options_autonotes_'.$note_type]);
                    sort($new_autonotes_field);
                    sort($actual_autonotes_field);
                    $arr_diff = array_diff($actual_autonotes_field, $new_autonotes_field);
                    if (!empty($arr_diff))
                    {
                        $lineowner->array_options['options_autonotes_'.$note_type] = implode(',', $new_autonotes_field);
                        $res = $lineowner->updateExtraField('autonotes_public');
                        if ($res < 1) {
                            dol_syslog(__METHOD__.' update extrafield autonotes_'.$note_type.' failed', LOG_ERR);
                        } else {
                            dol_syslog(__METHOD__.' auto remove autonote with this ids='.implode(',', $arr_diff).' after line deletion.');
                            $update_autonote = true;
                            $autoadded_autonotes_ids[$note_type] = $lineowner->array_options['options_autonotes_'.$note_type];
                        }
                    }
                }

                if ($update_autonote) autoNoteUpdateNoteField($lineowner, $autoadded_autonotes_ids);
                return 1;
                break;
        }
        return 0;
    }
}
