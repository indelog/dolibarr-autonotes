<?php
/* Copyright (C) 2004-2018  Laurent Destailleur     <eldy@users.sourceforge.net>
 * Copyright (C) 2018-2019  Nicolas ZABOURI         <info@inovea-conseil.com>
 * Copyright (C) 2019       Frédéric France         <frederic.france@netlogic.fr>
 * Copyright (C) 2020 Maxime DEMAREST <contact@indelog.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * 	\defgroup   autonotes     Module AutoNotes
 *  \brief      AutoNotes module descriptor.
 *
 *  \file       htdocs/autonotes/core/modules/modAutoNotes.class.php
 *  \ingroup    autonotes
 *  \brief      Description and activation file for module AutoNotes
 */
include_once DOL_DOCUMENT_ROOT.'/core/modules/DolibarrModules.class.php';

/**
 *  Description and activation class for module AutoNotes
 */
class modAutoNotes extends DolibarrModules
{
    /**
     * Constructor. Define names, constants, directories, boxes, permissions
     *
     * @param DoliDB $db Database handler
     */
    public function __construct($db)
    {
        global $langs, $conf;
        $this->db = $db;

        $this->numero = 172700;
        $this->rights_class = 'autonotes';
        $this->family = "technic";
        $this->module_position = '90';
        $this->name = preg_replace('/^mod/i', '', get_class($this));
        $this->description = "Adding automatic notes in various documents.";
        $this->descriptionlong = "Allows you to automatically add pre-recorded notes to the creation or editing of various document such as order, invoice and propal.";
        $this->editor_name = 'Indelog';
        $this->editor_url = 'https://www.indelog.fr';
        $this->version = '2.1.0';
        $this->url_last_version = 'https://framagit.org/indelog/dolibarr-autonotes';

        $this->const_name = 'MAIN_MODULE_'.strtoupper($this->name);
        $this->picto = 'autonotes@autonotes';

        $this->module_parts = array(
            'triggers' => 1,
            'login' => 0,
            'substitutions' => 0,
            'menus' => 0,
            'tpl' => 0,
            'barcode' => 0,
            'models' => 0,
            'theme' => 0,
            'css' => array(
            ),
            'js' => array(
            ),
            'hooks' => array(
                'ordercard',
                'ordersuppliercard',
                'invoicecard',
                'invoicesuppliercard',
                'propalcard',
                'expeditioncard',
            ),
            'moduleforexternal' => 0,
        );

        $this->config_page_url = array("setup.php@autonotes");
        $this->hidden = false;
        $this->depends = array('modCategorie');
        $this->requiredby = array();
        $this->conflictwith = array();
        $this->langfiles = array("autonotes@autonotes");
        $this->phpmin = array(7, 3);
        $this->need_dolibarr_version = array(12);
        $this->warnings_activation = array();
        $this->warnings_activation_ext = array();

        // module use extrafields, advert the user if extrasfields are disabled
        if (!empty($conf->global->MAIN_EXTRAFIELDS_DISABLED))
        {
            $this->warnings_activation = array(
                'always' => $langs->trans('WarnDisabledExtraFields'),
            );
        }

        $this->const = array(
        );

        if (!isset($conf->autonotes) || !isset($conf->autonotes->enabled)) {
            $conf->autonotes = new stdClass();
            $conf->autonotes->enabled = 0;
        }

        $this->tabs = array();

        $this->dictionaries = array();

        $this->boxes = array();

        $this->cronjobs = array();

        $this->rights = array();
        $r = 1000;

        /* BEGIN MODULEBUILDER PERMISSIONS */
        $this->rights[$r][0] = $this->numero + $r;
        $this->rights[$r][1] = 'Use AutoNotes';
        $this->rights[$r][4] = 'use';
        $this->rights[$r][5] = '';
        $r++;
        $this->rights[$r][0] = $this->numero + $r;
        $this->rights[$r][1] = 'Create/Update/Delete AutoNotes';
        $this->rights[$r][4] = 'modify';
        $this->rights[$r][5] = '';
        $r++;
        /* END MODULEBUILDER PERMISSIONS */

        $this->menu = array();
        $r = 0;
        /* BEGIN MODULEBUILDER TOPMENU */
        $this->menu[$r++] = array(
            'fk_menu'=>'fk_mainmenu=tools',
            'type'=>'left',
            'titre'=>'ManageAutoNote',
            'leftmenu'=>'autonotesnotelist',
            'url'=>'/autonotes/note_list.php',
            'langs'=>'autonotes@autonotes',
            'position'=>1000 + $r,
            'enabled'=>'$conf->autonotes->enabled',
            'perms'=>'$user->rights->autonotes->use',
            'target'=>'',
            'user'=>0,
        );
        /* END MODULEBUILDER TOPMENU */
        /* BEGIN MODULEBUILDER LEFTMENU NOTE */
        $this->menu[$r++]=array(
            'fk_menu'=>'fk_mainmenu=autonotes',
            'type'=>'left',
            'titre'=>'List Note',
            'mainmenu'=>'autonotes',
            'leftmenu'=>'autonotes_note',
            'url'=>'/autonotes/note_list.php',
            'langs'=>'autonotes@autonotes',
            'position'=>1100+$r,
            'enabled'=>'$conf->autonotes->enabled',
            'perms'=>'1',
            'target'=>'',
            'user'=>2,
        );
        $this->menu[$r++]=array(
            'fk_menu'=>'fk_mainmenu=autonotes,fk_leftmenu=autonotes_note',
            'type'=>'left',
            'titre'=>'New Note',
            'mainmenu'=>'autonotes',
            'leftmenu'=>'autonotes_note',
            'url'=>'/autonotes/note_card.php?action=create',
            'langs'=>'autonotes@autonotes',
            'position'=>1100+$r,
            'enabled'=>'$conf->autonotes->enabled',
            'perms'=>'1',
            'target'=>'',
            'user'=>2
        );
		/* END MODULEBUILDER LEFTMENU NOTE */

        // Exports profiles provided by this module
        $r = 1;
        /* BEGIN MODULEBUILDER EXPORT NOTE */
        /* END MODULEBUILDER EXPORT NOTE */

        // Imports profiles provided by this module
        $r = 1;
        /* BEGIN MODULEBUILDER IMPORT NOTE */
        /* END MODULEBUILDER IMPORT NOTE */
    }

    /**
     *  Function called when module is enabled.
     *  The init function add constants, boxes, permissions and menus (defined in constructor) into Dolibarr database.
     *  It also creates data directories
     *
     *  @param      string  $options    Options when enabling module ('', 'noboxes')
     *  @return     int             	1 if OK, 0 if KO
     */
    public function init($options = '')
    {
        global $conf, $langs, $db;

        require_once DOL_DOCUMENT_ROOT.'/core/lib/functions2.lib.php';
        require_once DOL_DOCUMENT_ROOT.'/core/lib/admin.lib.php';

        $result = $this->_load_tables('/autonotes/sql/');
        if ($result < 0) return -1;

        // Create extrafields during init
        include_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
        $extrafields = new ExtraFields($this->db);

        // Store autonotes setted on a object
        $result00=$extrafields->addExtraField('autonotes_public', 'AutoNotePublic', 'chkbxlst', 953,  255, 'commande', 0, 0, '', array('options'=>array('autonotes_note:ref:rowid::status=1 AND type=0'=>'')), 0, '$user->rights->autonotes->use', -1, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result01=$extrafields->addExtraField('autonotes_public', 'AutoNotePublic', 'chkbxlst', 953,  255, 'expedition', 0, 0, '', array('options'=>array('autonotes_note:ref:rowid::status=1 AND type=0'=>'')), 0, '$user->rights->autonotes->use', -1, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result02=$extrafields->addExtraField('autonotes_public', 'AutoNotePublic', 'chkbxlst', 953,  255, 'facture', 0, 0, '', array('options'=>array('autonotes_note:ref:rowid::status=1 AND type=0'=>'')), 0, '$user->rights->autonotes->use', -1, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result03=$extrafields->addExtraField('autonotes_public', 'AutoNotePublic', 'chkbxlst', 953,  255, 'propal', 0, 0, '', array('options'=>array('autonotes_note:ref:rowid::status=1 AND type=0'=>'')), 0, '$user->rights->autonotes->use', -1, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result04=$extrafields->addExtraField('autonotes_public', 'AutoNotePublic', 'chkbxlst', 953,  254, 'commande_fournisseur', 0, 0, '', array('options'=>array('autonotes_note:ref:rowid::status=1 AND type=0'=>'')), 0, '$user->rights->autonotes->use', -1, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result05=$extrafields->addExtraField('autonotes_public', 'AutoNotePublic', 'chkbxlst', 953,  255, 'facture_fourn', 0, 0, '', array('options'=>array('autonotes_note:ref:rowid::status=1 AND type=0'=>'')), 0, '$user->rights->autonotes->use', -1, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');

        // Register the html generated by autonote to avoid to modify it when the autonote is modified after if added on the objec
        $result10=$extrafields->addExtraField('autonotes_public_html', 'AutoNotePublicHtml', 'text', 1953, 0, 'commande', 0, 0, '', '', 0, '$user->rights->autonotes->use', 0, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result11=$extrafields->addExtraField('autonotes_public_html', 'AutoNotePublicHtml', 'text', 1953,  0, 'expedition', 0, 0, '', '', 0, '$user->rights->autonotes->use', 0, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result12=$extrafields->addExtraField('autonotes_public_html', 'AutoNotePublicHtml', 'text', 1953,  0, 'facture', 0, 0, '', '', 0, '$user->rights->autonotes->use', 0, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result13=$extrafields->addExtraField('autonotes_public_html', 'AutoNotePublicHtml', 'text', 1953,  0, 'propal', 0, 0, '', '', 0, '$user->rights->autonotes->use', 0, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result14=$extrafields->addExtraField('autonotes_public_html', 'AutoNotePublicHtml', 'text', 1953,  0, 'commande_fournisseur', 0, 0, '', '', 0, '$user->rights->autonotes->use', 0, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');
        $result15=$extrafields->addExtraField('autonotes_public_html', 'AutoNotePublicHtml', 'text', 1953,  0, 'facture_fourn', 0, 0, '', '', 0, '$user->rights->autonotes->use', 0, '', '', '', 'autonotes@autonotes', '$conf->autonotes->enabled');

        $sql = array();

        /*
         * Migrating
         */

        // If autonote version is not registed the version is less than 2, execute sommes updates
        dol_syslog(__METHOD__.' autonote last activated version : '.$conf->global->AUTONOTES_LAST_ACTIVATED_VERSION, LOG_DEBUG);
        if (empty($conf->global->AUTONOTES_LAST_ACTIVATED_VERSION))
        {
            dol_syslog(__METHOD__.' migrating to version 2.0.0');

            // disable it for the moment : it won't work properly
            $result110=$extrafields->delete('autonotes_public', 'expedition');

            // these fields are no longer used
            $result110=$extrafields->delete('autonotes_private', 'commande');
            $result110=$extrafields->delete('autonotes_private', 'facture');
            $result111=$extrafields->delete('autonotes_private', 'propal');
            $result112=$extrafields->delete('autonotes_private', 'expedition');
            $result113=$extrafields->delete('autonotes_private', 'commande_fournisseur');
            $result114=$extrafields->delete('autonotes_private', 'facture_fourn');

            // as the autonote_private is not used change set all autonote with type 1 (private) to type 0 (public) and disable them
            $sql[] = 'UPDATE '.MAIN_DB_PREFIX.'autonotes_note set type = 0, status = 0 WHERE type = 1;';

            // New column added in the autonotes_note table
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_soc_commande varchar(255) AFTER type;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_soc_expedition varchar(255) AFTER auto_categs_soc_commande;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_soc_facture varchar(255) AFTER auto_categs_soc_expedition;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_soc_propal varchar(255) AFTER auto_categs_soc_facture;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_soc_commande_fournisseur varchar(255) AFTER auto_categs_soc_propal;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_soc_facture_fourn varchar(255) AFTER auto_categs_soc_commande_fournisseur;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_product_commande varchar(255) AFTER auto_categs_soc_facture_fourn;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_product_expedition varchar(255) AFTER auto_categs_product_commande;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_product_facture varchar(255) AFTER auto_categs_product_expedition;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_product_propal varchar(255) AFTER auto_categs_product_facture;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_product_commande_fournisseur varchar(255) AFTER auto_categs_product_propal;';
            $sql[] = 'ALTER TABLE llx_autonotes_note ADD auto_categs_product_facture_fourn varchar(255) AFTER auto_categs_product_commande_fournisseur;';
        }


        if (versioncompare(explode('.', $conf->global->AUTONOTES_LAST_ACTIVATED_VERSION), array(2, 1 , 0)) < 0) {
            dol_syslog(__METHOD__.' migrating to version 2.1.0');
            $sql[] = "UPDATE llx_extrafields SET type = 'html' WHERE elementtype IN ('commande', 'expedition', 'facture', 'propal', 'commande_fournisseur', 'facture_fourn') AND name = 'autonotes_public_html'";
        }


        // update module version with the actual version
        dolibarr_set_const($this->db, 'AUTONOTES_LAST_ACTIVATED_VERSION', $this->version, 'chaine', 0, 'Rember last autonote installed version (usefull to correctly execute migrating script)', 0);

        return $this->_init($sql, $options);
    }

    /**
     *  Function called when module is disabled.
     *  Remove from database constants, boxes and permissions from Dolibarr database.
     *  Data directories are not deleted
     *
     *  @param      string	$options    Options when enabling module ('', 'noboxes')
     *  @return     int                 1 if OK, 0 if KO
     */
    public function remove($options = '')
    {
        $sql = array();
        return $this->_remove($sql, $options);
    }
}
