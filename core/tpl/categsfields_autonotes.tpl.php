<?php
/* Copyright (C) 2021      Maxime DEMAREST      <maxime@indelog.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *   	\file       /core/tpl/categsfields_autonotes.tpl.php
 *		\ingroup    autonotes
 *		\brief      Template to print categories selectors for autonotes
 */

/*
 * Need to have following variables defined:
 * $object (autnote Note)
 * $langs
 * $form (Form instance)
 */

// Protection to avoid direct call of template
if (empty($object) || ! $object instanceof Note)
{
	print "Error, template page can't be called as URL";
	exit;
}

?>

<!-- BEGIN PHP TEMPLATE categsfield_autonotes.tpl.php -->
<?php
print '<hr/>';
print load_fiche_titre($langs->trans('CategsForAutoAdd'), '', '');

print '<table class="border centpercent tableforfieldedit">'."\n";
foreach ($object::CATEGS_FIELDS as $field)
{
    preg_match("/^(?:auto_categs)_([a-z]+)_(.+)$/", $field, $elems);
    if ($elems[1] === 'product') $type_categ = Categorie::TYPE_PRODUCT;
    elseif ($elems[1] === 'soc')
    {
        if (in_array($elems[2], array('commande_fournisseur', 'facture_fourn'))) $type_categ = Categorie::TYPE_SUPPLIER;
        else $type_categ = Categorie::TYPE_CUSTOMER;
    }

    print '<tr id="field_'.$field.'">';
    print '<td>'.$form->textwithpicto($langs->trans('Field_'.$field), $langs->trans('Field_'.$field.'_Help')).'</td>';
    $selcted = 0;
    print '<td>'.$form->multiselectarray($field, $form->select_all_categories($type_categ, "", "", 64, 0, 1), GETPOSTISSET($field) ? GETPOST($field, 'array') : explode(',', $object->$field), 0, 0, "minwidth300").'<td/>';
    print '</tr>';
}
print '</table>';
?>
<!-- BEGIN PHP TEMPLATE categsfield_autonotes.tpl.php -->
