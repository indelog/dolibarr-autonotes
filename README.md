# AUTONOTES FOR [DOLIBARR ERP CRM](https://www.dolibarr.org)

Automatically add pre-registered mentions on propals, orders (customer and supplier) and invoices (customer and supplier)

## Features

Offer the possibility to create and manage several mentions (called *Autonotes*) which can be attached in case by case to the generated documents (only for propal, orders (customer and supplier) and invoices (customer and supplier) for the moment).

The *Autonotes* can be added manually as well automatically depending of the categories of customer or supplier at the object creation or depending of the product categories when a line is added to the object.

Other modules are available on [Dolistore.com](https://www.dolistore.com>).

## Dependence

This module depends on the category module to provide all its functionality. It can be used without but the automatic add for Autonotes won't work. Also it display a warning on the autnotes card. To disable this warning you can set the parameter `AUTONOTE_REMOVE_CATEG_MOD_NOT_ENBALED_WARN` to `1` on the *Other Setup* page.

## Installation

### From the ZIP file and GUI interface

- If you get the module in a zip file (like when downloading it from the market place [Dolistore](https://www.dolistore.com)), go into
menu ```Home - Setup - Modules - Deploy external module``` and upload the zip file.

### From a GIT repository

- Clone the repository in ```$dolibarr_main_document_root_alt/autonotes```

```sh
cd ....../custom
git clone git@framagit.org:indelog/dolibarr-autonotes.git
```

### <a name="final_steps"></a>Final steps

From your browser:

  - Log into Dolibarr as a super-administrator
  - Go to "Setup" -> "Modules"
  - You should now be able to find and enable the module

## Use

  - Go to "Tools" -> "Manage automatic notes" and add new *Autonote*. 
  - When creating new order, invoice, or propal, you can add automatic notes by selectig it in "Public automatic note".
    - If you have configured your *Autonote* for automatically add it depending the categories of customer or supplier, the "Public automatic note" field is automatically filled with the *Autonote* if the customer or supplier is in the corresponding categories.
    - You can add several automatic notes
    - You can delete or add new automatic note after document creation by modifying "Public automatic note" field.

### About the Autnotes auto remove

If you have configured your *Autonote* for automatically add it depending the categorie of the product present on the line added and if the *Autonote* not be manually or automatically added at the object creation, it will be automatically removed when the line is deleted if no other product in the remaining lines has a categorie which add this *Autonote*.

### Modify the content of an Autonote after added it on a document

To avoid retroactive change on the content of an *Autonotes* allready added to an object (if you modify the content of the *Autonote* later), the content of the *Autonote* is stored to the object when attached to it. If you want update the content of the *Autonote* present in the document with the new content, remove and re-add it from the object.

## Licenses

### Main code

GPLv3. See file COPYING for more information.

### Documentation

All texts and readmes are licensed under GFDL.
