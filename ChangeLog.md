# CHANGELOG AUTONOTES FOR [DOLIBARR ERP CRM](https://www.dolibarr.org)

## [Unreleased]
- A tab on object card for show html autonote field.
- Add a new type of Autonote for add if on mentions on page bottom.
- Add a new type of Autonote which is used to add a terms of use on a new page on generated document
- Substitution variable in Autonotes
- Correctly implement hooks an triggers on the module (permit others to add autonote on non suported object)
- Unit test
- Import/Export autonotes.
- Extrafileds support.
- External user support.

## [2.0.1] - 2021-02-22
- Fix v12 compatibility
- Fix the use of dedicated extrafield to put the autonote html content on the document (also fix autonote priority issue)

## [2.0.0] - 2021-02-22
- Abandonment the use of object note field. Instead use an dedicated extrafield named 'autonotes_public_html') to store the content of the Autonote (because ckeditor used to edit note field remove html comments used to store the id of Autonote).
- Remove the use of the private notes (useless).
- Autnote can now automatically added depending on the customer or supplier categories when the object is created or product categories when a line is added to the object.
- Remove the use of Autonote on shipment.
- Real view for Autonote card (not only edit view)k
- Autonote type disabled (because private fields no longer used)

## [1.0.1] - 2020-05-20
### Fixed
- Fix : add 1000 to id for specifc user rights for this module (avoid erase other rights)

## [1.0.0] - 2020-05-09
### Added
- Add private notes extrafields.
- en-US lang.

### Changed
- Fix remove added notes after autonote when remove autonotes.
- Fix autonotes not added whith shipping.

## [0.0.1]
Initial version with only public note functional (not publied).
