# AUTONOTES POUR [DOLIBARR ERP CRM](https://www.dolibarr.org)

Permet l'ajout de mentions pré enregistrés sur les documents générés pour les propositions, commandes (client et fournisseur) et factures (client et fournisseur).

## Caractéristiques

Offre la possibilité de créer et gérer plusieurs mentions (appelés *Autonotes*) qui peuvent êtres ajouté au cas sur les document générées (pour le moment seulement sur les propositions, commandes (client et fournisseur) et factures (client et fournisseur). 

Les *Autonotes* peuvent soit être ajoutée manuellement ou bien automatiquement en fonction des catégories du client ou du fournisseur à la création du objet ou bien en fonction de la catégorie du produit à l'ajout d'une ligne.

Retrouvez d'autres modules pour Dolibarr sur [Dolistore.com](https://www.dolistore.com>).

## Installation

### Depuis le fichier ZIP et l'interface graphique

- Si vous obtenez ce module via un fichier ZIP (ce qui est le cas si vous le récupérer depuis le [Dolistore](https://www.dolistore.com)), rendez-vous le menu ```Accueil - Configuration - Modules/Applications - Déployer/Installer un module externe``` et téléverser le fichier .zip.

### Depuis le dépôt GIT

- Cloner le dépot dans ```$dolibarr_main_document_root_alt/autonotes```

```sh
cd ....../custom
git clone git@framagit.org:indelog/dolibarr-autonotes.git
```

### <a name="final_steps"></a>Étape final

Depuis votre navigateur :

  - Connectez-vous à votre instance Dolibarr avec un compte super administrateur
  - Rendez-vous dans "Configuration" -> "Modules/Applications"
  - Vous devriez voire et être en mesure d'activer le module.

## Usage

  - Rendez-vous dans le menu "Outils" -> "Gérer les notes automatiques" et ajouter de nouvelle *Autonotes*. 
  - Lorsque vous créer de nouvelles commandes, factures, ou proposition commercial, vous pouvez ajouter vos *Autonotes* via le champ "Notes publique automatique".
    - Si vous avez configuré votre *Autonote* pour qu'elle soit automatiquement selon si le client ou le fournisseur se trouve dans une certaine catégorie, elle sera automatiquement ajouté à la création de l'objet.
    - Vous pouvez ajouter plusieurs notes automatiques.
    - Vous pouvez supprimer ou ajouter les notes automatique après la création de la ressource en modifiant les champs "Notes publique automatique" et "Notes privé automatique".

### A propos des suppression automatique d'Autonotes

Si vous avez configuré votre *Autonote* pour qu'elle s'ajoute automatiquement en fonction de la catégorie du produit ajouté lors de l'ajout d'une ligne et qu'elle n'a pas été au préalable ajouté manuellement ou automatiquement à la création de l'objet, celle ci sera automatiquement retiré si la ligne est supprimé et qu'aucun autre produit présent dans les lignes soit dans une catégorie qui ajoute l'*Autonote*.

### Modifié une Autonote déjà ajouté sur un document

Pour éviter les modifications rétroactive du contenus d'*Autonotes* déjà ajouté sur un document (si vous modifiez une *Autonote* à posteriori), le contenus de l'*Autonote* attaché au document est mémorisé tel qu'il était au moment de l'ajout sur celui-ci sur l'objet. Si vous voulez que se contenus soit à jours avec le nouveau, retirez l'*Autonote* en questions de la liste des *Autonotes* attaché à l'objet et ré ajoutez la.

## Licences

### Code principale

GPLv3. Voire le fichier COPYING pour plus d'information.

### Documentation

Tout textes et documentations sont sous licence GFDL.
