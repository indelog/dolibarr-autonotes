<?php
/* Copyright (C) 2017  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) 2020-2021 Maxime DEMAREST <maxime@indelog.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file        class/note.class.php
 * \ingroup     autonotes
 * \brief       This file is a CRUD class file for Note (Create/Read/Update/Delete)
 */

// Put here all includes required by your class file
require_once DOL_DOCUMENT_ROOT.'/core/class/commonobject.class.php';

/**
 * Class for Note
 */
class Note extends CommonObject
{
	/**
	 * @var string ID to identify managed object
	 */
	public $element = 'note';

	/**
	 * @var string Name of table without prefix where object is stored
	 */
	public $table_element = 'autonotes_note';

	/**
	 * @var int  Does note support multicompany module ? 0=No test on entity, 1=Test with field entity, 2=Test with link by societe
	 */
	public $ismultientitymanaged = 0;

	/**
	 * @var int  Does object support extrafields ? 0=No, 1=Yes
	 */
	public $isextrafieldmanaged = 0;

	/**
	 * @var string String with name of icon for note. Must be the part after the 'object_' into object_note.png
	 */
	public $picto = 'note@autonotes';

    const TYPE_NOTE_PUBLIC = 0;

    /**
     * @var string Map numertique type of note to a equivalent string
     */
    const MAP_TYPE_NOTE = array(
        self::TYPE_NOTE_PUBLIC => 'public',
    );

	/**
	 *  'type' if the field format ('integer', 'integer:ObjectClass:PathToClass[:AddCreateButtonOrNot[:Filter]]', 'varchar(x)', 'double(24,8)', 'real', 'price', 'text', 'html', 'date', 'datetime', 'timestamp', 'duration', 'mail', 'phone', 'url', 'password')
	 *         Note: Filter can be a string like "(t.ref:like:'SO-%') or (t.date_creation:<:'20160101') or (t.nature:is:NULL)"
	 *  'label' the translation key.
	 *  'enabled' is a condition when the field must be managed.
	 *  'position' is the sort order of field.
	 *  'notnull' is set to 1 if not null in database. Set to -1 if we must set data to null if empty ('' or 0).
	 *  'visible' says if field is visible in list (Examples: 0=Not visible, 1=Visible on list and create/update/view forms, 2=Visible on list only, 3=Visible on create/update/view form only (not list), 4=Visible on list and update/view form only (not create). 5=Visible on list and view only (not create/not update). Using a negative value means field is not shown by default on list but can be selected for viewing)
	 *  'noteditable' says if field is not editable (1 or 0)
	 *  'default' is a default value for creation (can still be overwrote by the Setup of Default Values if field is editable in creation form). Note: If default is set to '(PROV)' and field is 'ref', the default value will be set to '(PROVid)' where id is rowid when a new record is created.
	 *  'index' if we want an index in database.
	 *  'foreignkey'=>'tablename.field' if the field is a foreign key (it is recommanded to name the field fk_...).
	 *  'searchall' is 1 if we want to search in this field when making a search from the quick search button.
	 *  'isameasure' must be set to 1 if you want to have a total on list for this field. Field type must be summable like integer or double(24,8).
	 *  'css' is the CSS style to use on field. For example: 'maxwidth200'
	 *  'help' is a string visible as a tooltip on field
	 *  'showoncombobox' if value of the field must be visible into the label of the combobox that list record
	 *  'disabled' is 1 if we want to have the field locked by a 'disabled' attribute. In most cases, this is never set into the definition of $fields into class, but is set dynamically by some part of code.
	 *  'arraykeyval' to set list of value if type is a list of predefined values. For example: array("0"=>"Draft","1"=>"Active","-1"=>"Cancel")
	 *  'comment' is not used. You can store here any text of your choice. It is not used by application.
	 *
	 *  Note: To have value dynamic, you can set value to 0 in definition and edit the value on the fly into the constructor.
	 */

	// BEGIN MODULEBUILDER PROPERTIES
	/**
	 * @var array  Array with all fields and their property. Do not use it as a static var. It may be modified by constructor.
	 */
	public $fields=array(
		'rowid' => array('type'=>'integer', 'label'=>'TechnicalID', 'enabled'=>'1', 'position'=>1, 'notnull'=>1, 'visible'=>0, 'index'=>1, 'comment'=>"Id"),
		'ref' => array('type'=>'varchar(128)', 'label'=>'Ref', 'enabled'=>'1', 'position'=>10, 'notnull'=>1, 'visible'=>1, 'index'=>1, 'searchall'=>1,),
		'content' => array('type'=>'html', 'label'=>'NoteContent', 'enabled'=>'1', 'position'=>70, 'notnull'=>1, 'visible'=>1,),
		'priority' => array('type'=>'integer', 'label'=>'Priority', 'enabled'=>'1', 'position'=>80, 'notnull'=>1, 'visible'=>1, 'help'=>"PriorityHelp",),
        // type of note is no longet used for the moment
		'type' => array('type'=>'sellist', 'label'=>'Type', 'enabled'=>'0', 'position'=>90, 'notnull'=>1, 'default'=>'0', 'visible'=>1, 'index'=>1, 'arrayofkeyval'=>array('0'=>'Public'),),
		'status' => array('type'=>'sellist', 'label'=>'Status', 'enabled'=>'1', 'position'=>150, 'notnull'=>1, 'visible'=>1, 'default'=>'1', 'index'=>1, 'arrayofkeyval'=>array('0'=>'Disabled', '1'=>'Enabled'),),
		'date_creation' => array('type'=>'datetime', 'label'=>'DateCreation', 'enabled'=>'1', 'position'=>500, 'notnull'=>1, 'visible'=>-2,),
		'date_modification' => array('type'=>'datetime', 'label'=>'DateModification', 'enabled'=>'1', 'position'=>501, 'notnull'=>0, 'visible'=>-2, 'css'=>'center',),
		'tms' => array('type'=>'timestamp', 'label'=>'DateModification', 'enabled'=>'1', 'position'=>501, 'notnull'=>0, 'visible'=>-2,),
		'fk_user_creat' => array('type'=>'integer:User:user/class/user.class.php', 'label'=>'UserAuthor', 'enabled'=>'1', 'position'=>510, 'notnull'=>1, 'visible'=>-2, 'foreignkey'=>'user.rowid',),
		'fk_user_modif' => array('type'=>'integer:User:user/class/user.class.php', 'label'=>'UserModif', 'enabled'=>'1', 'position'=>511, 'notnull'=>-1, 'visible'=>-2,),
		'import_key' => array('type'=>'varchar(14)', 'label'=>'ImportId', 'enabled'=>'1', 'position'=>1000, 'notnull'=>-1, 'visible'=>-2,),
        /*
         * Fields to register which categorie of thirdparty ou product the auto-note must be automatically applied
         *
         * TODO do not use CommonObjet::showInputField to show these fileds for now (v 13.0.0) because the 'chkbxlst' no implement options for select element
         *      this is why the field has visible to 0 (managed directly by script).
         *      /!\ The note_card.php use From::multiselectarray() which post an array so we must convert the array in intcoma (with implode) for self::create() and self::update() before call CommonObject::createCommon() or CommonObject::updateCommon() because this field has varchar type.
        */
        // society
		'auto_categs_soc_commande' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_soc_commande', 'enabled'=>'1', 'position'=>100, 'notnull'=>0, 'visible'=>0,),
		'auto_categs_soc_expedition' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_soc_expedition', 'enabled'=>'1', 'position'=>101, 'notnull'=>0, 'visible'=>0,),
		'auto_categs_soc_facture' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_soc_facture', 'enabled'=>'1', 'position'=>102, 'notnull'=>0, 'visible'=>0,),
		'auto_categs_soc_propal' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_soc_propal', 'enabled'=>'1', 'position'=>103, 'notnull'=>0, 'visible'=>0,),
		'auto_categs_soc_commande_fournisseur' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_soc_commande_fournisseur', 'enabled'=>'1', 'position'=>104, 'notnull'=>0, 'visible'=>0,),
		'auto_categs_soc_facture_fourn' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_soc_facture_fourn', 'enabled'=>'1', 'position'=>105, 'notnull'=>0, 'visible'=>0,),
        // product
		'auto_categs_product_commande' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_product_commande', 'enabled'=>'1', 'position'=>110, 'notnull'=>0, 'visible'=>0,),
		'auto_categs_product_facture' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_product_facture', 'enabled'=>'1', 'position'=>112, 'notnull'=>0, 'visible'=>0,),
		'auto_categs_product_propal' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_product_propal', 'enabled'=>'1', 'position'=>113, 'notnull'=>0, 'visible'=>0,),
		'auto_categs_product_commande_fournisseur' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_product_commande_fournisseur', 'enabled'=>'1', 'position'=>114, 'notnull'=>0, 'visible'=>0,),
		'auto_categs_product_facture_fourn' => array('type'=>'varchar(255)', 'label'=>'Field_auto_categs_product_facture_fourn', 'enabled'=>'1', 'position'=>115, 'notnull'=>0, 'visible'=>0,),
	);
	public $rowid;
	public $ref;
	public $content;
	public $priority;
	public $type;
	public $status;
	public $date_creation;
	public $date_modification;
	public $tms;
	public $fk_user_creat;
	public $fk_user_modif;
	public $import_key;
	public $auto_categs_soc_commande;
	//public $auto_categs_soc_expedition;
	public $auto_categs_soc_facture;
	public $auto_categs_soc_propal;
	public $auto_categs_soc_commande_fournisseur;
	public $auto_categs_soc_facture_fourn;
    public $auto_categs_product_commande;
    public $auto_categs_product_facture;
    public $auto_categs_product_propal;
    public $auto_categs_product_commande_fournisseur;
    public $auto_categs_product_facture_fourn;
	// END MODULEBUILDER PROPERTIES

	/**
	 * @var int    Field with ID of parent key if this field has a parent
	 */
	public $fk_element = 'fk_autonote_note';

	/**
	 * @var array    List of field that posted value is an array and type is varchar and who need to be converted in intcoma before createCommon an updateCommon
	 */
    public const CATEGS_FIELDS = array(
        'auto_categs_soc_commande',
        'auto_categs_soc_expedition',
        'auto_categs_soc_facture',
        'auto_categs_soc_propal',
        'auto_categs_soc_commande_fournisseur',
        'auto_categs_soc_facture_fourn',
        'auto_categs_product_commande',
        'auto_categs_product_facture',
        'auto_categs_product_propal',
        'auto_categs_product_commande_fournisseur',
        'auto_categs_product_facture_fourn',
    );

	/**
	 * Constructor
	 *
	 * @param DoliDb $db Database handler
	 */
	public function __construct(DoliDB $db)
	{
		global $conf, $langs;

		$this->db = $db;

		if (empty($conf->global->MAIN_SHOW_TECHNICAL_ID) && isset($this->fields['rowid'])) $this->fields['rowid']['visible'] = 0;
		if (empty($conf->multicompany->enabled) && isset($this->fields['entity'])) $this->fields['entity']['enabled'] = 0;

		// Unset fields that are disabled
		foreach ($this->fields as $key => $val)
		{
			if (isset($val['enabled']) && empty($val['enabled']))
			{
				unset($this->fields[$key]);
			}
		}

		// Translate some data of arrayofkeyval
		if (is_object($langs))
		{
			foreach($this->fields as $key => $val)
			{
				if (is_array($val['arrayofkeyval']))
				{
					foreach($val['arrayofkeyval'] as $key2 => $val2)
					{
						$this->fields[$key]['arrayofkeyval'][$key2]=$langs->trans($val2);
					}
				}
			}
		}
	}

	/**
	 * Create object into database
	 *
	 * @param  User $user      User that creates
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 * @return int             <0 if KO, Id of created object if OK
	 */
	public function create(User $user, $notrigger = false)
	{
        // The posted value is an array and the field has varchar type, so we must convert it in intcoma before createCommon
        foreach(self::CATEGS_FIELDS as $field)
        {
            if(!empty($this->$field) && is_array($this->$field))
            {
                $this->$field = implode(',', $this->$field);
            }
        }
		return $this->createCommon($user, $notrigger);
	}

	/**
	 * Create a clone of this object
	 *
	 * @param  	User 	$user      	User that creates
	 * @param  	int 	$newref     The new reference of the object
	 * @return 	mixed 				New created object of <1 if error
	 */
	public function createClone(User $user, $newref = '')
	{
		global $langs;
		$error = 0;

		dol_syslog(__METHOD__, LOG_DEBUG);

        if (empty($this->id))
        {
            dol_syslog(__METHOD__.' : need to fetch object before clone it.', LOG_ERR);
            return -1;
        }

		$clone = clone $this;

		// Reset some properties
		unset($clone->id);
		unset($clone->fk_user_creat);
		unset($clone->import_key);

		// Clear fields
		$clone->ref = empty($newref) ? 'Copy_Of_'.$this->ref : $newref;
		$clone->status =  $this->status;
		$clone->date_creation = dol_now();
		$clone->date_modification = null;

		// Create clone
		$clone->context['createfromclone'] = 'createfromclone';
		$result = $clone->create($user);
		if ($result < 0) {
			$error++;
			$this->error = $clone->error;
			$this->errors = $clone->errors;
		}

		unset($clone->context['createfromclone']);

		if (!$error) {
			return $clone;
		} else {
			return -5;
		}
	}

	/**
	 * Load object in memory from the database
	 *
	 * @param int    $id   Id object
	 * @param string $ref  Ref
	 * @return int         <0 if KO, 0 if not found, >0 if OK
	 */
	public function fetch($id, $ref = null)
	{
		$result = $this->fetchCommon($id, $ref);
		return $result;
	}


	/**
	 * Load list of objects in memory from the database.
	 *
	 * @param  string      $sortorder    Sort Order
	 * @param  string      $sortfield    Sort field
	 * @param  int         $limit        limit
	 * @param  int         $offset       Offset
	 * @param  array       $filter       Filter array. Example array('field'=>'valueforlike', 'customurl'=>...)
	 * @param  string      $filtermode   Filter mode (AND or OR)
	 * @return array|int                 int <0 if KO, array of pages if OK
	 */
	public function fetchAll($sortorder = '', $sortfield = '', $limit = 0, $offset = 0, array $filter = array(), $filtermode = 'AND')
	{
		global $conf;

		dol_syslog(__METHOD__, LOG_DEBUG);

		$records = array();

		$sql = 'SELECT ';
		$sql .= $this->getFieldList();
		$sql .= ' FROM '.MAIN_DB_PREFIX.$this->table_element.' as t';
		if (isset($this->ismultientitymanaged) && $this->ismultientitymanaged == 1) $sql .= ' WHERE t.entity IN ('.getEntity($this->table_element).')';
		else $sql .= ' WHERE 1 = 1';
		// Manage filter
		$sqlwhere = array();
		if (count($filter) > 0) {
			foreach ($filter as $key => $value) {
				if ($key == 't.rowid') {
					$sqlwhere[] = $key.'='.$value;
				}
				elseif (strpos($key, 'date') !== false) {
					$sqlwhere[] = $key.' = \''.$this->db->idate($value).'\'';
				}
				elseif ($key == 'customsql') {
					$sqlwhere[] = $value;
				}
				else {
					$sqlwhere[] = $key.' LIKE \'%'.$this->db->escape($value).'%\'';
				}
			}
		}
		if (count($sqlwhere) > 0) {
			$sql .= ' AND ('.implode(' '.$filtermode.' ', $sqlwhere).')';
		}

		if (!empty($sortfield)) {
			$sql .= $this->db->order($sortfield, $sortorder);
		}
		if (!empty($limit)) {
			$sql .= ' '.$this->db->plimit($limit, $offset);
		}

		$resql = $this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
            $i = 0;
			while ($i < min($limit, $num))
			{
			    $obj = $this->db->fetch_object($resql);

				$record = new self($this->db);
				$record->setVarsFromFetchObj($obj);

				$records[$record->id] = $record;

				$i++;
			}
			$this->db->free($resql);

			return $records;
		} else {
			$this->errors[] = 'Error '.$this->db->lasterror();
			dol_syslog(__METHOD__.' '.join(',', $this->errors), LOG_ERR);

			return -1;
		}
	}

	/**
	 * Update object into database
	 *
	 * @param  User $user      User that modifies
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 * @return int             <0 if KO, >0 if OK
	 */
	public function update(User $user, $notrigger = false)
	{
        // The posted value is an array and the field has varchar type, so we must convert it in intcoma before createCommon
        foreach(self::CATEGS_FIELDS as $field)
        {
            if(is_array($this->$field))
            {
                $this->$field = implode(',', $this->$field);
                if (empty($this->field)) $this->field = null;
            }
        }
		return $this->updateCommon($user, $notrigger);
	}

	/**
	 * Delete object in database
	 *
	 * @param User $user       User that deletes
	 * @param bool $notrigger  false=launch triggers after, true=disable triggers
	 * @return int             <0 if KO, >0 if OK
	 */
	public function delete(User $user, $notrigger = false)
	{
		return $this->deleteCommon($user, $notrigger);
	}

    /**
     *  Return a link to the object card (with optionaly the picto)
     *
     *  @param  int     $withpicto                  Include picto in link (0=No picto, 1=Include picto into link, 2=Only picto)
     *  @param  string  $option                     On what the link point to ('nolink', ...)
     *  @param  int     $notooltip                  1=Disable tooltip
     *  @param  string  $morecss                    Add more css on link
     *  @param  int     $save_lastsearch_value      -1=Auto, 0=No save of lastsearch_values when clicking, 1=Save lastsearch_values whenclicking
     *  @return	string                              String with URL
     */
    public function getNomUrl($withpicto = 0, $option = '', $notooltip = 0, $morecss = '', $save_lastsearch_value = -1)
    {
        global $conf, $langs, $hookmanager;

        if (!empty($conf->dol_no_mouse_hover)) $notooltip = 1; // Force disable tooltips

        $label .= '<b>'.$langs->trans('Ref').':</b>&nbsp:'.$this->ref;
        $label .= '</br><b>'.$langs->trans('Status').'&nbsp:</b>'.$this->getLibStatut(5);
        $label .= '</br><b>'.$langs->trans('Type').'&nbsp:</b>&nbsp'.$this->fields['type']['arrayofkeyval'][$this->type];
        $label .= '</br><b>'.$langs->trans('Priority').'&nbsp:</b>&nbsp:'.$this->priority;
        $label .= '</br><u>'.$langs->trans('NoteContent').'</u></br>'.$this->content;

        $url = dol_buildpath('/autonotes/note_card.php', 1).'?id='.$this->id;

        if ($option != 'nolink')
        {
            // Add param to save lastsearch_values or not
            $add_save_lastsearch_values = ($save_lastsearch_value == 1 ? 1 : 0);
            if ($save_lastsearch_value == -1 && preg_match('/list\.php/', $_SERVER["PHP_SELF"])) $add_save_lastsearch_values = 1;
            if ($add_save_lastsearch_values) $url .= '&save_lastsearch_values=1';
        }

        $linkclose = '';
        if (empty($notooltip))
        {
            if (!empty($conf->global->MAIN_OPTIMIZEFORTEXTBROWSER))
            {
                $label = $langs->trans("ShowNote");
                $linkclose .= ' alt="'.dol_escape_htmltag($label, 1).'"';
            }
            // Must clean lines breaks added in content field else we see it on the tooltip
            // TODO dol_string_nounprintableascii not implemented in v12 uncomment this line later
            //$linkclose .= ' title="'.dol_escape_htmltag(dol_string_nounprintableascii($label), 1).'"';
            $linkclose .= ' title="'.dol_escape_htmltag(preg_replace('/[\x00-\x1F\x7F]/u', '', $label), 1).'"';
            $linkclose .= ' class="classfortooltip'.($morecss ? ' '.$morecss : '').'"';
        }
        else $linkclose = ($morecss ? ' class="'.$morecss.'"' : '');

		$linkstart = '<a href="'.$url.'"';
		$linkstart .= $linkclose.'>';
		$linkend = '</a>';

		$result .= $linkstart;
		if ($withpicto) $result .= img_object(($notooltip ? '' : $label), ($this->picto ? $this->picto : 'generic'), ($notooltip ? (($withpicto != 2) ? 'class="paddingright"' : '') : 'class="'.(($withpicto != 2) ? 'paddingright ' : '').'classfortooltip"'), 0, 0, $notooltip ? 0 : 1);
		if ($withpicto != 2) $result .= $this->ref;
		$result .= $linkend;

		global $action, $hookmanager;
		$hookmanager->initHooks(array('notedao'));
		$parameters = array('id'=>$this->id, 'getnomurl'=>$result);
		$reshook = $hookmanager->executeHooks('getNomUrl', $parameters, $this, $action); // Note that $action and $object may have been modified by some hooks
		if ($reshook > 0) $result = $hookmanager->resPrint;
		else $result .= $hookmanager->resPrint;

		return $result;
    }

	/**
	 *  Return label of the status
	 *
	 *  @param  int		$mode          0=long label, 1=short label, 2=Picto + short label, 3=Picto, 4=Picto + long label, 5=Short label + Picto, 6=Long label + Picto
	 *  @return	string 			       Label of status
	 */
	public function getLibStatut($mode = 0)
	{
		return $this->LibStatut($this->status, $mode);
	}

    // phpcs:disable PEAR.NamingConventions.ValidFunctionName.ScopeNotCamelCaps
	/**
	 *  Return the status
	 *
	 *  @param	int		$status        Id status
	 *  @param  int		$mode          0=long label, 1=short label, 2=Picto + short label, 3=Picto, 4=Picto + long label, 5=Short label + Picto, 6=Long label + Picto
	 *  @return string 			       Label of status
	 */
	public function LibStatut($status, $mode = 0)
	{
		// phpcs:enable
		if (empty($this->labelStatus) || empty($this->labelStatusShort))
		{
			global $langs;
			//$langs->load("autonotes");
			$this->labelStatus[1] = $langs->trans('Enabled');
			$this->labelStatus[0] = $langs->trans('Disabled');
			$this->labelStatusShort[1] = $langs->trans('Enabled');
			$this->labelStatusShort[0] = $langs->trans('Disabled');
		}

		if ($status == 1) $statusType = 'status4';
	    else $statusType = 'status9';

		return dolGetStatus($this->labelStatus[$status], $this->labelStatusShort[$status], '', $statusType, $mode);
	}

	/**
	 *	Load the info information in the object
	 *
	 *	@param  int		$id       Id of object
	 *	@return	void
	 */
	public function info($id)
	{
		$sql = 'SELECT rowid, date_creation as datec, tms as datem,';
		$sql .= ' fk_user_creat, fk_user_modif';
		$sql .= ' FROM '.MAIN_DB_PREFIX.$this->table_element.' as t';
		$sql .= ' WHERE t.rowid = '.$id;
		$result = $this->db->query($sql);
		if ($result)
		{
			if ($this->db->num_rows($result))
			{
				$obj = $this->db->fetch_object($result);
				$this->id = $obj->rowid;

                $cuser = new User($this->db);
                $cuser->fetch($obj->fk_user_creat);
                $this->user_creation = $cuser;

                $muser = new User($this->db);
                $muser->fetch($obj->fk_user_modif);
                $this->user_modif = $muser;

				$this->date_creation     = $this->db->jdate($obj->datec);
				$this->date_modification = $this->db->jdate($obj->datem);
			}

			$this->db->free($result);
		}
		else
		{
			dol_print_error($this->db);
		}
	}

	/**
	 * Initialise object with example values
	 * Id must be 0 if object instance is a specimen
	 *
	 * @return void
	 */
	public function initAsSpecimen()
	{
		$this->initAsSpecimenCommon();
	}

	/**
	 * Action executed by scheduler
	 * CAN BE A CRON TASK. In such a case, parameters come from the schedule job setup field 'Parameters'
	 *
	 * @return	int			0 if OK, <>0 if KO (this function is used also by cron so only 0 is OK)
	 */
	//public function doScheduledJob($param1, $param2, ...)
	public function doScheduledJob()
    {
        return 0;
	}

}
