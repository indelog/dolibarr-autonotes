<?php

/* Copyright (C) 2021 Maxime DEMAREST <maxime@indelog.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    autonotes/class/actions_autonotes.class.php
 * \ingroup autonotes
 * \brief   Hooks used by autonotes modules
 */

/**
 * Class ActionsAutoNotes
 */
class ActionsAutoNotes
{
    /**
     * @var DoliDB Database handler.
     */
    public $db;

    /**
     * @var string Error code (or message)
     */
    public $error = '';

    /**
     * @var array Errors
     */
    public $errors = array();


    /**
     * @var array Hook results. Propagated to $hookmanager->resArray for later reuse
     */
    public $results = array();

    /**
     * @var string String displayed by executeHook() immediately after return
     */
    public $resprints;


    /**
     * Constructor
     *
     *  @param      DoliDB      $db      Database handler
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Hook before the form on card is printed to automatically append the autonote at object creation
     *
     * @param   array   $parameters            Array of parameters
     * @param   CommonObject    $object        The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
     * @param   string  $action                'add', 'update', 'view'
     * @return  int                            <0 if KO,
     *                                         =0 if OK but we want to process standard actions too,
     *                                         >0 if OK and we want to replace standard actions.
     */
    public function formObjectOptions($parameters, &$object, &$action)
    {
        global $conf;

        // The auto append autonotes is only for object creation
        if ($action == 'create') {
            // If the module categories not enable, do not autoadd autonotes base on thirdpary categories
            if (!$conf->categorie->enabled) {
                dol_syslog(__METHOD__ . ' autonotes was not automatically added because the module Categorie was not enabled.', LOG_WARNING);
                return 0;
            }
            require_once __DIR__ . '/../lib/autonotes.lib.php';

            global $conf, $user, $langs;
            global $hookmanager;

            $outputlangs = $langs;

            dol_syslog(__METHOD__ . ' action=' . $action . ' currentcontext=' . $parameters['currentcontext'], LOG_DEBUG);


            // (see autoNoteGetForObj() in lib/autonotes.lib.php for the details)
            $autonote_ctx = '';
            switch ($parameters['currentcontext']) {
                case 'ordercard':
                    $autonote_ctx = 'commande';
                    break;
                case 'ordersuppliercard':
                    $autonote_ctx = 'commande_fournisseur';
                    break;
                case 'invoicecard':
                    $autonote_ctx = 'facture';
                    break;
                case 'invoicesuppliercard':
                    $autonote_ctx = 'facture_fourn';
                    break;
                case 'propalcard':
                    $autonote_ctx = 'propal';
                    break;
                case 'expeditioncard':
                    $autonote_ctx = 'expedition';
                    break;
            }

            // automatically add autonote only
            if (!empty($autonote_ctx)) {

                // For facture and exepdition originating from expedition, don't get soc id.
                // This permit the autonotes inerited form the command.
                if (in_array($autonote_ctx, array('expedition', 'facture')) && GETPOST('origin', 'aZ') === 'commande') {
                    $socid = "";
                } else {
                    // TODO the hook formObjectOptions properly set the socid whith parameters but y need to use showOptional in place because facture fourn does not use forObjectOptions
                    //      Think to implement this hook in facture fourn
                    $socid = $parameters['socid'];
                    $socid = GETPOST('socid', 'int');
                }

                // only add autonote when socid is loaded (else we can't get the customer categories
                if (empty($socid)) {
                    dol_syslog(__METHOD__ . ' no socid', LOG_DEBUG);
                } else {
                    $autonotes_ids = autoNoteGetForObj((int) $socid, $this->db, 'soc', $autonote_ctx);
                    $object->array_options = empty($object->array_options) ? $autonotes_ids : array_merge($object->array_options, $autonotes_ids);
                    dol_syslog(__METHOD__ . ' auto append autonotes whith this ids=' . $autonotes_ids . ' for the autonote context ' . $autonote_ctx . ' for the thirdpary with id=' . $socid, LOG_DEBUG);
                }
            } elseif (empty($autonote_ctx)) {
                dol_syslog(__METHOD__ . ' have no autonotes context for this hook context : ' . $parameters['currentcontext'], LOG_DEBUG);
            }
        }

        return 0;
    }

    /**
     * This hook is responsible of show autonotes on documents by add autonotes on $object->note_public
     *
     * @param   array           $parameters     Array of parameters
     * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
     * @param   string          $action         'add', 'update', 'view'
     * @return  int                              <0 if KO,
     *                                           =0 if OK but we want to process standard actions too,
     *                                           >0 if OK and we want to replace standard actions.
     */
    public function commonGenerateDocument($parameters, &$object, &$action)
    {
        // if has not autonotes do nothing
        if (empty($object->array_options['options_autonotes_public_html'])) {
            return 0;
        }

        dol_syslog(__METHOD__ . ' added autonote with ids=' . $object->array_options['options_autonotes_public'], LOG_DEBUG);
        $object->note_public .= '<div>' . $object->array_options['options_autonotes_public_html'] . '</div>';
        return 0;
    }
}
