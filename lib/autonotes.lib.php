<?php
/* Copyright (C) 2020 Maxime DEMAREST <contact@indelog.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    autonotes/lib/autonotes.lib.php
 * \ingroup autonotes
 * \brief   Library files with common functions for AutoNotes
 */

/**
 * Prepare admin pages header
 *
 * @return array
 */
function autonotesAdminPrepareHead()
{
	global $langs, $conf;

	$langs->load("autonotes@autonotes");

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/autonotes/admin/setup.php", 1);
	$head[$h][1] = $langs->trans("Settings");
	$head[$h][2] = 'settings';
	$h++;
	$head[$h][0] = dol_buildpath("/autonotes/admin/about.php", 1);
	$head[$h][1] = $langs->trans("About");
	$head[$h][2] = 'about';
	$h++;

	complete_head_from_modules($conf, $langs, null, $head, $h, 'autonotes');

	return $head;
}

/**
 * Return the list of autonotes to automaticaly append for an object function
 * of its categories
 *
 * @param   int     $id             The id of the object which the autonote is attached.
 * @param   DoliDb  $db             Database handler.
 * @param   string  $ctx1           First context element that determine autonote Note categories field to use. May  be one of :
 *                                  - 'soc' for thirdparty categorie (for add autonote at the object creation)
 *                                  - 'product' for add autonote when add product line dependent to the product categorie.
 * @param   string  $ctx2           Second element context that determine the field used to find the categorie of an element to apply autonotes (on of this : commande, expedition, facture, propal, commande_fournisseur, facture_fournisseur).
 * @param   boolean $return_comma   It it true the ids of autnotes are return in comma sepearated string else it's an array
 * @return  array                   Ids of autonotes whish must by applied for public and private note.
 */
function autoNoteGetForObj(int $id, DoliDB $db, string $ctx1, string $ctx2, $return_comma = true)
{
    dol_syslog(__FUNCTION__.' id='.$id.' ctx1='.$ctx1.' ctx2='.$ctx2, LOG_DEBUG);

    require_once __DIR__.'/../class/note.class.php';
    $static_note = new Note($db);
    $auto_categ_field = 'auto_categs_'.$ctx1.'_'.$ctx2;

    if (! in_array($auto_categ_field, $static_note::CATEGS_FIELDS))
    {
        dol_syslog(__FUNCTION__.' : '.$auto_categ_field.' not present in Note::CATEGS_FIELDS', LOG_ERR);
        return -1;
    }

    // Retrive an array indexed whith each autonotes ids and whith autonote type and array of associated categories as values
    $sql = 'SELECT rowid AS id, type, '.$auto_categ_field.' AS field FROM '.MAIN_DB_PREFIX.$static_note->table_element.' WHERE '.$auto_categ_field.' <> \'\' AND status = 1';
    $resql = $db->query($sql);
    if (!$resql)
    {
        $errmgs =
        dol_print_error($db, __FUNCTION__.' : failed to query autonote for auto_categ_field='.$auto_categ_field);
        $db->close();
        exit();
    }
    $autonotes_ids = array();
    while($res = $db->fetch_object($resql)) $autonotes_ids[$res->id] = array('type' => $res->type, 'field' => explode(',', $res->field));

    // Retrive each categories which the object is member
    require_once DOL_DOCUMENT_ROOT.'/categories/class/categorie.class.php';
    $catobj = new Categorie($db);
    // if is customer or supplier is depending of ctx2 else alway is product
    if ($ctx1 === 'soc')
    {
        switch ($ctx2)
        {
            case 'commande_fournisseur':
            case 'facture_fourn':
                $categtype = 'supplier';
                break;
            default:
                $categtype = 'customer';
                break;
        }
    }
    else
    {
        $categtype = $ctx1;
    }
    $catlist = $catobj->containing($id, $categtype, 'id');
    // if the object has not categories we not need to continue
    if (empty($catlist)) return array();

    // Finaly find the compare the categories of each autonote whith the categories of the object and append the autonote id of the returned array if if match
    // For now only the public type is keeped
    $autonotes_public = array();
    foreach ($autonotes_ids as $k => $v)
    {
        if (!empty(array_intersect($catlist, $v['field']))) {
            if ($v['type'] == Note::TYPE_NOTE_PUBLIC) $autonotes_public[$k] = (string) $k;
        }
    }

    $db->free($resql);

    // TODO implement Note::MAP_TYPE_NOTE for returned array
    if ($return_comma) return array('options_autonotes_public' => implode(',', $autonotes_public));
    else return array('options_autonotes_public' => $autonotes_public);
}


/**
 * Update the fileld that register the html for autonote (this is what is added on the generated documents)
 *
 * @param   CommonObject     $object            Objet which we want update note field
 * @param   Array            $autoadded_ids     The ids of autonotes that automatically added when insert new line
 * @return  int                                 Allway return 0
 */
function autoNoteUpdateNoteField(CommonObject &$object, $autoadded_ids = array())
{
    global $db;
    require_once __DIR__.'../../class/note.class.php';
    dol_syslog(__FUNCTION__, LOG_DEBUG);
    // Only public type used for the moment
    foreach(Note::MAP_TYPE_NOTE as $typenote) {
        if (empty($autoadded_ids[$typenote])) $autoadded_ids[$typenote] = array();
        $arr_autonotes = array();
        $note_static = new Note($db);

        // Get array of all autonotes used by this object
        if (!empty($object->array_options['options_autonotes_'.$typenote])) {;
            $sql = 'SELECT rowid AS id, priority, content FROM '.MAIN_DB_PREFIX.$note_static->table_element.' WHERE rowid IN ('.$object->array_options['options_autonotes_'.$typenote].') ORDER BY priority DESC';

            $resql = $db->query($sql);
            if ($resql < 1)
            {
                dol_syslog(__FUNCTION__.' faild to query autonotes.', LOG_ERR);
                continue;
            }

            while ($res = $db->fetch_object($resql))
            {
                // TODO dol_string_nounprintableascii not implemented in v12 replce preg_mathc when unsuport v12
                //$clean_str = dol_string_nounprintableascii($res->content);
                $clean_str = preg_replace('/[\x00-\x1F\x7F]/u', '', $res->content);
                // remove the last fucking &nbsp; added by ckeditor automatically added after the last <br/>
                preg_match('/(^.*)(<br[ ]?[\/]?>)(&nbsp;)/', $clean_str, $match_str);
                $content_str = empty($match_str) ? $clean_str : $match_str[1].$match_str[2];
                $arr_autonotes[$res->id] = array(
                    'priority' => $res->priority,
                    'content' => $content_str,
                );
            }
        }

        // Split actual note with separate autonotes
        $note_elements = preg_split('/<!--AUTONOTES_BEGIN-->|<!--AUTONOTES_END-->/', $object->array_options['options_autonotes_'.$typenote.'_html']);

        foreach($note_elements as $elem) {
            if (preg_match('/^<!--AUTONOTES_NOTEID_([0-9]+)_([A-Z]+)-->(.*)$/', $elem, $n_params))
            {
                $n_id = $n_params[1];
                $n_added = $n_params[2];
                $n_content = $n_params[3];
                // If autonote has removed do not re add it
                if (empty($arr_autonotes[$n_id])) continue;
                // if autonote already has content keep the original content
                $arr_autonotes[$n_id]['content'] = $n_content;
                // keep the marker auto added if the autonote is autoadded
                if ($n_added === 'AUTO' && ! in_array($n_id, $autoadded_ids[$typenote]))
                    $autoadded_ids[$typenote][] = $n_id;
            }
        }

        // Set up autonote content
        foreach($arr_autonotes as $id=>$values) {
            $autoparam = (in_array($id, $autoadded_ids[$typenote])) ? 'AUTO' : 'MANU';
            $content .= '<!--AUTONOTES_BEGIN--><!--AUTONOTES_NOTEID_'.$id.'_'.$autoparam.'-->'.$values['content'].'<!--AUTONOTES_END-->';
        }


        dol_syslog(__FUNCTION__.' : update note '.$typenote.' for '.$object->element.' whith id '.$object->id);
        $object->array_options['options_autonotes_'.$typenote.'_html'] = $content ? $content : '';
        $object->updateExtraField('autonotes_'.$typenote.'_html');
    }
    return 0;
}
