<?php
/* Copyright (C) 2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *   	\file       note_card.php
 *		\ingroup    autonotes
 *		\brief      Page to create/edit/view note
 */

//if (! defined('NOREQUIREDB'))              define('NOREQUIREDB','1');					// Do not create database handler $db
//if (! defined('NOREQUIREUSER'))            define('NOREQUIREUSER','1');				// Do not load object $user
//if (! defined('NOREQUIRESOC'))             define('NOREQUIRESOC','1');				// Do not load object $mysoc
//if (! defined('NOREQUIRETRAN'))            define('NOREQUIRETRAN','1');				// Do not load object $langs
//if (! defined('NOSCANGETFORINJECTION'))    define('NOSCANGETFORINJECTION','1');		// Do not check injection attack on GET parameters
//if (! defined('NOSCANPOSTFORINJECTION'))   define('NOSCANPOSTFORINJECTION','1');		// Do not check injection attack on POST parameters
//if (! defined('NOCSRFCHECK'))              define('NOCSRFCHECK','1');					// Do not check CSRF attack (test on referer + on token if option MAIN_SECURITY_CSRF_WITH_TOKEN is on).
//if (! defined('NOTOKENRENEWAL'))           define('NOTOKENRENEWAL','1');				// Do not roll the Anti CSRF token (used if MAIN_SECURITY_CSRF_WITH_TOKEN is on)
//if (! defined('NOSTYLECHECK'))             define('NOSTYLECHECK','1');				// Do not check style html tag into posted data
//if (! defined('NOREQUIREMENU'))            define('NOREQUIREMENU','1');				// If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))            define('NOREQUIREHTML','1');				// If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))            define('NOREQUIREAJAX','1');       	  	// Do not load ajax.lib.php library
//if (! defined("NOLOGIN"))                  define("NOLOGIN",'1');						// If this page is public (can be called outside logged session). This include the NOIPCHECK too.
//if (! defined('NOIPCHECK'))                define('NOIPCHECK','1');					// Do not check IP defined into conf $dolibarr_main_restrict_ip
//if (! defined("MAIN_LANG_DEFAULT"))        define('MAIN_LANG_DEFAULT','auto');					// Force lang to a particular value
//if (! defined("MAIN_AUTHENTICATION_MODE")) define('MAIN_AUTHENTICATION_MODE','aloginmodule');		// Force authentication handler
//if (! defined("NOREDIRECTBYMAINTOLOGIN"))  define('NOREDIRECTBYMAINTOLOGIN',1);		// The main.inc.php does not make a redirect if not logged, instead show simple error message
//if (! defined("FORCECSP"))                 define('FORCECSP','none');					// Disable all Content Security Policies


// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME']; $tmp2 = realpath(__FILE__); $i = strlen($tmp) - 1; $j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) { $i--; $j--; }
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1))."/main.inc.php")) $res = @include substr($tmp, 0, ($i + 1))."/main.inc.php";
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php")) $res = @include dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php";
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) $res = @include "../main.inc.php";
if (!$res && file_exists("../../main.inc.php")) $res = @include "../../main.inc.php";
if (!$res && file_exists("../../../main.inc.php")) $res = @include "../../../main.inc.php";
if (!$res) die("Include of main fails");

require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT.'/categories/class/categorie.class.php';
dol_include_once('/autonotes/class/note.class.php');
dol_include_once('/autonotes/lib/autonotes_note.lib.php');

// Load translation files required by the page
$langs->loadLangs(array("autonotes@autonotes", "other"));

// Get parameters
$id = GETPOST('id', 'int');
$ref        = GETPOST('ref', 'alpha');
$action = GETPOST('action', 'aZ09');
$confirm    = GETPOST('confirm', 'alpha');
$cancel     = GETPOST('cancel', 'aZ09');
$contextpage = GETPOST('contextpage', 'aZ') ?GETPOST('contextpage', 'aZ') : 'notecard'; // To manage different context of search
$backtopage = GETPOST('backtopage', 'alpha');
$backtopageforcancel = GETPOST('backtopageforcancel', 'alpha');
//$lineid   = GETPOST('lineid', 'int');

// Initialize technical objects
$object = new Note($db);
$hookmanager->initHooks(array('notecard', 'globalcard')); // Note that conf->hooks_modules contains array

$search_array_options = $extrafields->getOptionalsFromPost($object->table_element, '', 'search_');

// Initialize array of search criterias
$search_all = trim(GETPOST("search_all", 'alpha'));
$search = array();
foreach ($object->fields as $key => $val)
{
	if (GETPOST('search_'.$key, 'alpha')) $search[$key] = GETPOST('search_'.$key, 'alpha');
}

if (empty($action) && empty($id) && empty($ref)) $action = 'view';

// Load object
include DOL_DOCUMENT_ROOT.'/core/actions_fetchobject.inc.php'; // Must be include, not include_once.
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';

// Security check - Protection if external user
if ($user->socid > 0) accessforbidden();
//if ($user->socid > 0) $socid = $user->socid;
//$isdraft = (($object->statut == $object::STATUS_DRAFT) ? 1 : 0);
//$result = restrictedArea($user, 'autonotes', $object->id, '', '', 'fk_soc', 'rowid', $isdraft);

$permissiontoread = $user->rights->autonotes->use;
$permissiontoadd = $user->rights->autonotes->modify; // Used by the include of actions_addupdatedelete.inc.php and actions_lineupdown.inc.php
$permissiontodelete = $user->rights->autonotes->modify;


/*
 * Actions
 */

$parameters = array();
$reshook = $hookmanager->executeHooks('doActions', $parameters, $object, $action); // Note that $action and $object may have been modified by some hooks
if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

if (empty($reshook))
{
    $error = 0;

    $backurlforlist = dol_buildpath('/autonotes/note_list.php', 1).'?restore_lastsearch_values=1';

    if (empty($backtopage))
    {
        if ($cancel)
            if (empty($id)) $backtopage = $backurlforlist;
            else $backtopage = dol_buildpath('/autonotes/note_card.php', 1).'?id='.$id.'&restore_lastsearch_values=1';
    }
    $triggermodname = 'AUTONOTES_NOTE_MODIFY'; // Name of trigger action code to execute when we modify record

    // Actions cancel, add, update, update_extras, confirm_validate, confirm_delete, confirm_deleteline, confirm_clone, confirm_close, confirm_setdraft, confirm_reopen
    // For this objet, only use add, update and confirm delete action
    if (in_array($action, array('add', 'update', 'confirm_delete')))
    {
        if ($action === 'update')
        {
            // TODO quik and dirty fix
            // This is to force remove categ if the field is set to empty
            // This is required because Form::multiselectarray() do not set empty value if we remove all selected value of the field so it post nothing and the value can't be updated to empty.
            foreach($object::CATEGS_FIELDS as $field)
            {
                if (! GETPOSTISSET($field)) $object->$field = array();
            }

        }

        include DOL_DOCUMENT_ROOT.'/core/actions_addupdatedelete.inc.php';
    }
    elseif ($action === 'confirm_clone' && $confirm === 'yes')
    {
        $newref = GETPOST('newref', 'az09');
        $nospecialref = dol_string_nospecial($newref);
        if (empty($newref) || $newref !== $nospecialref)
        {
            setEventMessages($langs->trans('IncorectNewRef', $nospecialref), '', 'errors');
            $action = 'clone';
        }
        else
        {
            if (empty($id) || $object->fetch($id) < 1)
            {
                header('Location'.$backtopage);
                exit();
            }
            $clone = $object->createClone($user, $newref);
            if ($clone instanceof Note)
            {
                $object = $clone;
                $action = 'edit';
            }
        }
    }

}




/*
 * View
 *
 * Put here all code to build page
 */

$form = new Form($db);

llxHeader('', $langs->trans('Note'), '');

// Part to create
if ($action == 'create')
{
	print load_fiche_titre($langs->trans('NewAutoNote'));

    print '<!--Start create form-->';
	print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
	print '<input type="hidden" name="token" value="'.newToken().'">';
	print '<input type="hidden" name="action" value="add">';
	if ($backtopage) print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
	if ($backtopageforcancel) print '<input type="hidden" name="backtopageforcancel" value="'.$backtopageforcancel.'">';

	dol_fiche_head(array(), '');

	print '<table class="border centpercent tableforfieldcreate">'."\n";

	// Common attributes
	include DOL_DOCUMENT_ROOT.'/core/tpl/commonfields_add.tpl.php';


	print '</table>'."\n";

    // function only availables when categories are enabled
    if ($conf->categorie->enabled)
    {
        // print the categories selectors for the autonotes
        include __DIR__.'/core/tpl/categsfields_autonotes.tpl.php';
    }
    else if (empty($conf->global->AUTONOTE_REMOVE_CATEG_MOD_NOT_ENBALED_WARN))
    {
        dol_syslog(__FILE__.' categories fields printed because the Categorie module is disabled.', LOG_WARNING);
        setEventMessages($langs->trans('WarnCategoriesNotEnabled'), '', 'warnings');
    }

	dol_fiche_end();

	print '<div class="center">';
	print '<input type="submit" class="button" name="add" value="'.dol_escape_htmltag($langs->trans("Create")).'">';
	print '&nbsp; ';
	print '<input type="'.($backtopage ? "submit" : "button").'" class="button" name="cancel" value="'.dol_escape_htmltag($langs->trans("Cancel")).'"'.($backtopage ? '' : ' onclick="javascript:history.go(-1)"').'>'; // Cancel for create does not post form if we don't know the backtopage
	print '</div>';

	print '</form>';
    print '<!--End create form-->';

	dol_set_focus('input[name="ref"]');
}

// Part to edit record (also show it)
elseif (($id || $ref))
{
    if ($action == 'edit')
    {
        print load_fiche_titre($langs->trans("EditAutoNote"));

        $formconfirm = '';

        // Confirmation to delete
        if ($action == 'delete')
        {
            $formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id, $langs->trans('DeleteNote'), $langs->trans('ConfirmDeleteObject'), 'confirm_delete', '', 0, 1);
        }

        // Call Hook formConfirm
        $parameters = array('formConfirm' => $formconfirm, 'lineid' => $lineid);
        $reshook = $hookmanager->executeHooks('formConfirm', $parameters, $object, $action); // Note that $action and $object may have been modified by hook
        if (empty($reshook)) $formconfirm .= $hookmanager->resPrint;
        elseif ($reshook > 0) $formconfirm = $hookmanager->resPrint;

        // Print form confirm
        print $formconfirm;

        print '<!--Start edit form-->';
        print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
        print '<input type="hidden" name="token" value="'.newToken().'">';
        print '<input type="hidden" name="action" value="update">';
        print '<input type="hidden" name="id" value="'.$object->id.'">';
        if ($backtopage) print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
        if ($backtopageforcancel) print '<input type="hidden" name="backtopageforcancel" value="'.$backtopageforcancel.'">';

        dol_fiche_head();

        print '<table class="border centpercent tableforfieldedit">'."\n";

        // Common attributes
        include DOL_DOCUMENT_ROOT.'/core/tpl/commonfields_edit.tpl.php';

        print '</table>';

        // function only availables when categories are enabled
        if ($conf->categorie->enabled)
        {
            // print the categories selectors for the autonotes
            include __DIR__.'/core/tpl/categsfields_autonotes.tpl.php';
        }
        else if (empty($conf->global->AUTONOTE_REMOVE_CATEG_MOD_NOT_ENBALED_WARN))
        {
            dol_syslog(__FILE__.' categories fields printed because the Categorie module is disabled.', LOG_WARNING);
            setEventMessages($langs->trans('WarnCategoriesNotEnabled'), '', 'warnings');
        }

        dol_fiche_end();

        print '<div class="center"><input type="submit" class="button" name="save" value="'.$langs->trans("Save").'">';
        print ' &nbsp; <input type="submit" class="button" name="cancel" value="'.$langs->trans("Cancel").'">';
        print '</div>';

        print '</form>';
        print '<!--End edit form-->';
    }
    else
    {
        $head = notePrepareHead($object);

	    print dol_get_fiche_head($head, 'card', $langs->trans('AutoNote'), -1, $object->picto);

        // Form confirmation
        if ($action == 'delete') {
            $formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id, $langs->trans('ConfirmDelete'), $langs->trans('ConfirmDeleteAutonote'), 'confirm_delete', '', 0, 1);
        }
        if ($action == 'clone') {
            $formquestion = array(
                array('name'=>'newref', 'label'=>$langs->trans('NewRef'), 'type'=>'text', 'morecss'=>'fieldrequired'),
            );
            $formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id, $langs->trans('ToClone'), $langs->trans('ConfirmCloneAsk', $object->ref), 'confirm_clone', $formquestion, 'yes', 1);
        }
        // Call Hook formConfirm
        $parameters = array('formConfirm' => $formconfirm, 'lineid' => $lineid);
        $reshook = $hookmanager->executeHooks('formConfirm', $parameters, $object, $action);
        if (empty($reshook)) $formconfirm .= $hookmanager->resPrint;
        elseif ($reshook > 0) $formconfirm = $hookmanager->resPrint;
        print $formconfirm;

        $linkback = '<a href="'.$backurlforlist.'">'.$langs->trans("BackToList").'</a>';
        $morehtmlref = '<div class="refidno">';
        $morehtmlref .= '</div>';

        dol_banner_tab($object, 'ref', $linkback, 1, 'ref', 'ref', $morehtmlref);

        print '<div class="fichecenter">';

        // disable view of not content because it displayed in full widht before
        $object->fields['content']['visible'] = 0;
        print '<!--Start show note content-->';
        print '<hr/>';
        print '<div class="valuefield fieldname_content" style="padding: 0 15px">'.$object->content.'</div>';
        print '<hr/>';
        print '<!--End show note content-->';
        print '<div class="clearboth"></div>';

        print '<!--Start show classic fields at letf column-->';
        print '<div class="fichehalfleft">';
        print '<div class="underbanner clearboth"></div>';
        print '<table class="border centpercent tableforfield">'."\n";

        foreach ($object->fields as $key => $val)
        {
            // Discard if extrafield is a hidden field on form
            if (abs($val['visible']) != 1 && abs($val['visible']) != 3 && abs($val['visible']) != 4 && abs($val['visible']) != 5) continue;

            if (array_key_exists('enabled', $val) && isset($val['enabled']) && !verifCond($val['enabled'])) continue; // We don't want this field
            if (in_array($key, array('ref', 'status'))) continue; // Ref and status are already in dol_banner

            $value = $object->$key;

            print '<tr><td';
            print ' class="titlefield fieldname_'.$key;
            //if ($val['notnull'] > 0) print ' fieldrequired';     // No fieldrequired on the view output
            if ($val['type'] == 'text' || $val['type'] == 'html') print ' tdtop';
            print '">';
            if (!empty($val['help'])) print $form->textwithpicto($langs->trans($val['label']), $langs->trans($val['help']));
            else print $langs->trans($val['label']);
            print '</td>';
            print '<td class="valuefield fieldname_'.$key;
            if ($val['type'] == 'text') print ' wordbreak';
            if ($val['cssview']) print ' '.$val['cssview'];
            print '">';
            if (in_array($val['type'], array('text', 'html'))) print '<div class="longmessagecut">';
            print $object->showOutputField($val, $key, $value, '', '', '', 0);
            //print dol_escape_htmltag($object->$key, 1, 1);
            if (in_array($val['type'], array('text', 'html'))) print '</div>';
            print '</td>';
            print '</tr>';
        }

        print '</table>';
        print '</div>';
        print '<!--End show classic fields at letf column-->';

        print '<div class="fichehalfright">';
        print '<div class="underbanner clearboth"></div>';

        // function only availables when categories are enabled
        if ($conf->categorie->enabled)
        {
            print '<!--Start show categories fields-->';
            // use the second column to display categories selectors
            print '<table class="border centpercent tableforfield">';

            foreach ($object::CATEGS_FIELDS as $key)
            {
                $value = array();
                if (!empty($object->$key))
                {
                    $catgs_ids = explode(',', $object->$key);
                    foreach ($catgs_ids as $catid)
                    {
                        $catobj = new Categorie($db);
                        $res = $catobj->fetch($catid);
                        if ($res > 0) $value[] = $catobj;
                    }
                }

                print '<tr>';
                print '<td class="titlefield fieldname_'.$key.'">'.$form->textwithpicto($langs->trans('Field_'.$key), $langs->trans('Field_'.$key.'_Help')).'</td>';
                print '<td class="valuefield fieldname_'.$key.'">';
                $toprint = array();
                foreach ($value as $c)
                {
                    $ways = $c->print_all_ways(' &gt;&gt; ', ($nolink ? 'none' : ''), 0, 1);
                    foreach ($ways as $way)
                    {
                        $toprint[] = '<li class="select2-search-choice-dolibarr noborderoncategories"'.($c->color ? ' style="background: #'.$c->color.';"' : ' style="background: #bbb"').'>'.$way.'</li>';
                    }
                }
                print '<div class="select2-container-multi-dolibarr" style="width: 90%;"><ul class="select2-choices-dolibarr">'.implode(' ', $toprint).'</ul></div>';
                print '</td></tr>';
            }

            print '</table>';
            print '<!--End show categories fields-->';

        }
        else if (empty($conf->global->AUTONOTE_REMOVE_CATEG_MOD_NOT_ENBALED_WARN))
        {
            dol_syslog(__FILE__.' categories fields printed because the Categorie module is disabled.', LOG_WARNING);
            setEventMessages($langs->trans('WarnCategoriesNotEnabled'), '', 'warnings');
        }

        print '</div>';
        print '<div class="clearboth"></div>';

        print '<!--Start tabs actions-->';
		print '<div class="tabsAction">'."\n";
		$parameters = array();
		$reshook = $hookmanager->executeHooks('addMoreActionsButtons', $parameters, $object, $action); // Note that $action and $object may have been modified by hook
		if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

		if (empty($reshook))
		{
			print dolGetButtonAction($langs->trans('Modify'), '', 'default', $_SERVER["PHP_SELF"].'?id='.$object->id.'&action=edit', '', $permissiontoadd);
			print dolGetButtonAction($langs->trans('ToClone'), '', 'default', $_SERVER['PHP_SELF'].'?id='.$object->id.'&socid='.$object->socid.'&action=clone&object=scrumsprint', '', $permissiontoadd);
			print dolGetButtonAction($langs->trans('Delete'), '', 'delete', $_SERVER['PHP_SELF'].'?id='.$object->id.'&action=delete', '', $permissiontodelete || ($object->status == $object::STATUS_DRAFT && $permissiontoadd));
        }
        print '<!--End tabs actions-->';
        print dol_get_fiche_end();

    }
}

// End of page
llxFooter();
$db->close();
