-- Copyright (C) Maxime DEMAREST <maxime@indelog.fr>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see https://www.gnu.org/licenses/.


CREATE TABLE llx_autonotes_note(
	-- BEGIN MODULEBUILDER FIELDS
	rowid integer AUTO_INCREMENT PRIMARY KEY NOT NULL, 
	ref varchar(128) NOT NULL, 
	content text NOT NULL, 
	priority integer NOT NULL, 
	type integer NOT NULL DEFAULT 0, 
	auto_categs_soc_commande varchar(255),
	auto_categs_soc_expedition varchar(255),
	auto_categs_soc_facture varchar(255),
	auto_categs_soc_propal varchar(255),
	auto_categs_soc_commande_fournisseur varchar(255),
	auto_categs_soc_facture_fourn varchar(255),
	auto_categs_product_commande varchar(255),
	auto_categs_product_expedition varchar(255),
	auto_categs_product_facture varchar(255),
	auto_categs_product_propal varchar(255),
	auto_categs_product_commande_fournisseur varchar(255),
	auto_categs_product_facture_fourn varchar(255),
	status integer DEFAULT 1 NOT NULL, 
	date_creation datetime NOT NULL, 
	date_modification datetime, 
	tms timestamp, 
	fk_user_creat integer NOT NULL, 
	fk_user_modif integer, 
	import_key varchar(14)
	-- END MODULEBUILDER FIELDS
) ENGINE=innodb;
